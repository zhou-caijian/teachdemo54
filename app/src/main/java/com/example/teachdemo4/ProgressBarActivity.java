package com.example.teachdemo4;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.teachdemo4.model.User;

import java.util.Random;

public class ProgressBarActivity extends AppCompatActivity {

    ProgressBar progressBar;
    Button btnSet,btnStop;
    int value = 0;
    boolean isStop = false;
    Random random = new Random();
    TextView tvMsg;
    Handler handler;//定义变量
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_progress_bar);
        progressBar = findViewById(R.id.progressBar);
        btnSet = findViewById(R.id.btnSet);
        btnStop = findViewById(R.id.btnStop);
        tvMsg = findViewById(R.id.tvMsg);
        progressBar.setProgress(value);
        //使用AlertDialog实现用户消息提示（交互按钮事件处理）
        handler = new Handler(){
            @Override
            public void handleMessage(@NonNull Message msg) {
                super.handleMessage(msg);
                if(msg.what == 0x110){
                    //Log.v("msg","空消息");

                    progressBar.setProgress(msg.arg1);

                    User user = (User) msg.obj;
                    tvMsg.setText("value("+ user.getUid()  +"):" + value);
                }

            }
        };
        btnSet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Log.v("ran:","" + (int)(Math.random() * 10));
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        while(!isStop){

                            value = value + random.nextInt(10);

                            /*
                            if(value >= 100){
                                progressBar.setProgress(100);
                                tvMsg.setText("value:" + 100);
                                break;
                            }*/
                            if(value >= 100){
                                value = 100;
                                isStop = true;
                            }
                            //Log.v("ran:","" + random.nextInt(10) + "value:" + value);
                            //tvMsg.setText("value:" + value);//老版本会出问题
                            //progressBar.setProgress(value);//老版本会出问题
                            if(handler != null){
                                //handler.sendEmptyMessage(0x110);
                                User user = new User("1001","zhang");
                                Message message = new Message();
                                message.what = 0x110;
                                message.arg1 = value;
                                message.obj = user;
                                handler.sendMessage(message);
                            }
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }

                        }
                    }
                }).start();
            }
        });
        btnStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.v("stop:","stop");
                isStop = true;
                progressBar.setProgress(value);

            }
        });

    }
}
