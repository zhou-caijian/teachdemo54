package com.example.teachdemo4;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class RecyclerViewActivity extends AppCompatActivity {

    RecyclerView rv_contact;

    int[] imgIds = new int[]{
            R.mipmap.img001,R.mipmap.img002,R.mipmap.img003,
            R.mipmap.img004,R.mipmap.img005,R.mipmap.img006,
            R.mipmap.img001,R.mipmap.img002,R.mipmap.img003,
            R.mipmap.img004,R.mipmap.img005,R.mipmap.img006,
            R.mipmap.img001,R.mipmap.img002,R.mipmap.img003,
            R.mipmap.img004,R.mipmap.img005,R.mipmap.img006,
            R.mipmap.img001,R.mipmap.img002,R.mipmap.img003,
            R.mipmap.img004,R.mipmap.img005,R.mipmap.img006,
            R.mipmap.img001,R.mipmap.img002,R.mipmap.img003,
            R.mipmap.img004,R.mipmap.img005,R.mipmap.img006,
            R.mipmap.img001,R.mipmap.img002,R.mipmap.img003,
            R.mipmap.img004,R.mipmap.img005,R.mipmap.img006,
            R.mipmap.img001,R.mipmap.img002,R.mipmap.img003,
            R.mipmap.img004,R.mipmap.img005,R.mipmap.img006,
            R.mipmap.img001,R.mipmap.img002,R.mipmap.img003,
            R.mipmap.img004,R.mipmap.img005,R.mipmap.img006
    };
    String[] strText = new String[]{
            "1111","2222","3333","4444","5555","6666",
            "1111","2222","3333","4444","5555","6666",
            "1111","2222","3333","4444","5555","6666",
            "1111","2222","3333","4444","5555","6666",
            "1111","2222","3333","4444","5555","6666",
            "1111","2222","3333","4444","5555","6666",
            "1111","2222","3333","4444","5555","6666",
            "1111","2222","3333","4444","5555","6666"
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recycler_view);

        rv_contact = findViewById(R.id.rv_contact);

        ContactAdapter adapter = new ContactAdapter();
        rv_contact.setAdapter(adapter);//设置数据适配器

        rv_contact.setLayoutManager(new LinearLayoutManager(RecyclerViewActivity.this));

    }
    class ContactViewHolder extends RecyclerView.ViewHolder{  //创建视图控制类

        TextView tv_contact;
        ImageView iv_contact;

        public ContactViewHolder(@NonNull View itemView) {
            super(itemView);
            tv_contact = itemView.findViewById(R.id.tv_contact);
            iv_contact = itemView.findViewById(R.id.iv_contact);
        }
    }
    class ContactAdapter extends RecyclerView.Adapter<ContactViewHolder>{   //适配器

        @NonNull
        @Override
        public ContactViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

            ContactViewHolder viewHolder = new ContactViewHolder(LayoutInflater.from(RecyclerViewActivity.this).inflate(R.layout.rv_item,parent,false));

            return viewHolder;
        }

        @Override
        public void onBindViewHolder(@NonNull ContactViewHolder holder, int position) {//数据绑定
            holder.iv_contact.setImageResource(imgIds[position]);
            holder.tv_contact.setText(strText[position]);
        }

        @Override
        public int getItemCount() {
            return imgIds.length;
        }
    }

}
