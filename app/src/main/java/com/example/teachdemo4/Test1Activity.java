package com.example.teachdemo4;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.view.View;
import android.widget.Button;

import com.example.teachdemo4.model.Student;
import com.example.teachdemo4.model.User;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Test1Activity extends AppCompatActivity {
    Button btn_go;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test1);

        btn_go = findViewById(R.id.btn_go);


        btn_go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //创建一个Intent对象，指定从源Activity（Test1Activity.this）到目标Activity（Test2Activty.class）
                Intent intent = new Intent(Test1Activity.this,Test2Activty.class);

                String strValue = "Android";
                int intValue = 1010;
                String[] strArray = new String[]{"111","222","333"};
                List<String> list = new ArrayList<>();
                list.add("aaa");
                list.add("bbb");
                list.add("ccc");

                User user = new User("10001","zhangsan");

                Student user1 = new Student("10001","zhangsan");
                Student user2 = new Student("10002","lisi");
                Student user3 = new Student("10003","wangwu");

                ArrayList<Student> userList = new ArrayList<>();
                userList.add(user1);
                userList.add(user2);
                userList.add(user3);

                Bundle bundle = new Bundle();
                bundle.putInt("int_key",intValue);
                bundle.putString("str_key",strValue);
                bundle.putStringArray("array_key",strArray);
                bundle.putStringArrayList("list_key", (ArrayList<String>) list);

                bundle.putSerializable("user_key", user);


                bundle.putParcelableArrayList("par_key", userList);
                intent.putExtras(bundle);

                startActivity(intent);//启动Activity

                finish();//关闭当前activity


            }
        });
    }
}
