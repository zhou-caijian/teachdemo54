package com.example.teachdemo4;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ListViewActivity extends AppCompatActivity {

    ListView lv_contact;
    int[] imgIds = new int[]{
            R.mipmap.img001,R.mipmap.img002,R.mipmap.img003,
            R.mipmap.img004,R.mipmap.img005,R.mipmap.img006,
            R.mipmap.img001,R.mipmap.img002,R.mipmap.img003,
            R.mipmap.img004,R.mipmap.img005,R.mipmap.img006,
            R.mipmap.img001,R.mipmap.img002,R.mipmap.img003,
            R.mipmap.img004,R.mipmap.img005,R.mipmap.img006,
            R.mipmap.img001,R.mipmap.img002,R.mipmap.img003,
            R.mipmap.img004,R.mipmap.img005,R.mipmap.img006,
            R.mipmap.img001,R.mipmap.img002,R.mipmap.img003,
            R.mipmap.img004,R.mipmap.img005,R.mipmap.img006,
            R.mipmap.img001,R.mipmap.img002,R.mipmap.img003,
            R.mipmap.img004,R.mipmap.img005,R.mipmap.img006,
            R.mipmap.img001,R.mipmap.img002,R.mipmap.img003,
            R.mipmap.img004,R.mipmap.img005,R.mipmap.img006,
            R.mipmap.img001,R.mipmap.img002,R.mipmap.img003,
            R.mipmap.img004,R.mipmap.img005,R.mipmap.img006
    };
    String[] strText = new String[]{
            "1111","2222","3333","4444","5555","6666",
            "1111","2222","3333","4444","5555","6666",
            "1111","2222","3333","4444","5555","6666",
            "1111","2222","3333","4444","5555","6666",
            "1111","2222","3333","4444","5555","6666",
            "1111","2222","3333","4444","5555","6666",
            "1111","2222","3333","4444","5555","6666",
            "1111","2222","3333","4444","5555","6666"
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view);

        lv_contact = findViewById(R.id.lv_contact);


        List<Map<String,String>> list = new ArrayList<>();

        for(int m=0;m<imgIds.length;m++){

            Map<String,String> map = new HashMap<>();

            map.put("myImg",String.valueOf(imgIds[m]));
            map.put("name",strText[m]);

            list.add(map);
        }


        String[] from = new String[]{"myImg","name"};

        int[] to = new int[]{R.id.img_contact,R.id.tv_contact};

        //SimpleAdapter adapter = new SimpleAdapter(ListViewActivity.this,list,R.layout.lv_item,from,to);

        ContactSimpleAdapter adapter = new ContactSimpleAdapter(ListViewActivity.this,list,R.layout.lv_item,from,to);

        lv_contact.setAdapter(adapter);

    }

    class ContactSimpleAdapter extends SimpleAdapter{

        Context context;
        int resource;
        public ContactSimpleAdapter(Context context, List<? extends Map<String, ?>> data, int resource, String[] from, int[] to) {
            super(context, data, resource, from, to);
            this.context = context;
            this.resource = resource;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            convertView = LayoutInflater.from(context).inflate(resource,parent,false);

            TextView textView = convertView.findViewById(R.id.tv_contact);

            if(position%6==0){

                textView.setTextColor(Color.RED);

            }else {
                textView.setTextColor(Color.GRAY);
            }

            return super.getView(position, convertView, parent);

        }
    }

}
