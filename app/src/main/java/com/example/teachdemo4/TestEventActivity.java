package com.example.teachdemo4;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class TestEventActivity extends AppCompatActivity implements View.OnClickListener {//第三种实现方式---类实现View.OnClickListener接口

    Button btn_envent1,btn_envent2,btn_envent3,btn_envent4;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_event);
        btn_envent1 = findViewById(R.id.btn_envent1);
        btn_envent2 = findViewById(R.id.btn_envent2);
        btn_envent3 = findViewById(R.id.btn_envent3);
        btn_envent4 = findViewById(R.id.btn_envent4);



        //第一种实现方式---非匿名
        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(view.getId() == R.id.btn_envent1){

                }
                if(view.getId() == R.id.btn_envent2){

                }
                if(view.getId() == R.id.btn_envent3){

                }
                if(view.getId() == R.id.btn_envent4){

                }
            }
        };

        btn_envent1.setOnClickListener(listener);
        btn_envent2.setOnClickListener(listener);
        btn_envent3.setOnClickListener(listener);
        btn_envent4.setOnClickListener(listener);
        //第一种实现方式---匿名对象
        btn_envent1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        btn_envent2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        btn_envent3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        btn_envent4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        //第三种实现方式---监听
        btn_envent1.setOnClickListener(this);
        btn_envent2.setOnClickListener(this);
        btn_envent3.setOnClickListener(this);
        btn_envent4.setOnClickListener(this);

        //第四种实现方式---匿名方式
        btn_envent1.setOnClickListener(new OnButtonClickListener() {
            @Override
            public void onMyClick(View view) {

            }

            @Override
            public void onClick(View view) {

            }
        });
        btn_envent2.setOnClickListener(new OnButtonClickListener() {
            @Override
            public void onMyClick(View view) {

            }

            @Override
            public void onClick(View view) {

            }
        });

        //第四种实现方式---非匿名方式
        OnButtonClickListener buttonClickListener = new OnButtonClickListener() {
            @Override
            public void onMyClick(View view) {

            }

            @Override
            public void onClick(View view) {

            }
        };

        btn_envent1.setOnClickListener(buttonClickListener);
    }
    //第二种实现方式
    public void enventClick(View view) {

        if(view.getId() == R.id.btn_envent1){

        }
        if(view.getId() == R.id.btn_envent2){

        }
        if(view.getId() == R.id.btn_envent3){

        }
        if(view.getId() == R.id.btn_envent4){

        }
    }
    //第三种实现方式---类实现View.OnClickListener接口--重写onClick方法
    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.btn_envent1){

        }
        if(view.getId() == R.id.btn_envent2){

        }
        if(view.getId() == R.id.btn_envent3){

        }
        if(view.getId() == R.id.btn_envent4){

        }
    }

}
//第四种实现方式---自定义接口
interface OnButtonClickListener extends View.OnClickListener{
    void onMyClick(View view);
}




















































