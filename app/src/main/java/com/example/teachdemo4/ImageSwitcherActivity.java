package com.example.teachdemo4;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageSwitcher;
import android.widget.ImageView;
import android.widget.ViewSwitcher;

public class ImageSwitcherActivity extends AppCompatActivity {

    int[] imgIds = new int[]{
            R.mipmap.img001,R.mipmap.img002,R.mipmap.img003,
            R.mipmap.img004,R.mipmap.img005,R.mipmap.img006
    };
    ImageSwitcher iSwitcher;
    float downX,upX;
    int currentIndex=2;

    Gallery gallery;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_switcher);
        iSwitcher = findViewById(R.id.iSwitcher);
        iSwitcher.setFactory(new ViewSwitcher.ViewFactory() {
            @Override
            public View makeView() {

                ImageView imageView = new ImageView(ImageSwitcherActivity.this);


                imageView.setImageResource(imgIds[2]);

                return imageView;
            }
        });

        iSwitcher.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                if(motionEvent.getAction()  == MotionEvent.ACTION_DOWN){
                    downX = motionEvent.getX();
                    return true;
                }else if(motionEvent.getAction()  == MotionEvent.ACTION_UP){

                    upX = motionEvent.getX();

                    if( downX - upX > 10 ){

                        Log.v("left","left");
                        currentIndex = currentIndex -1;
                        //if(currentIndex < 0) currentIndex = 0;
                        if(currentIndex <0 ) currentIndex = imgIds.length - 1;
                        iSwitcher.setImageResource(imgIds[currentIndex]);
                    }

                    if( upX - downX > 10 ){
                        Log.v("right","right");
                        currentIndex = currentIndex + 1;
                        //if(currentIndex < 0) currentIndex = 0;
                        if(currentIndex == imgIds.length ) currentIndex = 0;
                        iSwitcher.setImageResource(imgIds[currentIndex]);

                    }

                    return true;

                }

                return false;
            }
        });


        gallery = findViewById(R.id.gallery);

        BaseAdapter adapter = new BaseAdapter() {
            @Override
            public int getCount() {//获取item数量
                return imgIds.length;
            }
            @Override
            public Object getItem(int position) {
                return imgIds[position];
            }
            @Override
            public long getItemId(int position) {

                return position;
            }
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                ImageView imageView  ;

                if(convertView == null){

                    imageView = new ImageView(ImageSwitcherActivity.this);
                    imageView.setPadding(50,50,50,50);
                }else {
                    imageView = (ImageView) convertView;
                }
                imageView.setImageResource(imgIds[position]);
                return imageView;
            }
        };

        gallery.setAdapter(adapter);
        gallery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Log.v("click","" +i);
            }
        });
        gallery.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {

                Log.v("long","" +i);
                return false;
            }
        });
        gallery.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Log.v("selected","" +i);

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Log.v("no","");
            }
        });


    }
}
