package com.example.teachdemo4;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class TestResourceActivity extends AppCompatActivity {
    TextView tv_msg;
    LinearLayout ll_layout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_resource);

        tv_msg = findViewById(R.id.tv_msg);
        ll_layout = findViewById(R.id.ll_layout);

        tv_msg.setText(R.string.my_title2);

        tv_msg.setBackgroundColor(getResources().getColor(R.color.colorAccent) );
        String[] strArray = getResources().getStringArray(R.array.str_array);
        String strText = "";
        for(String str : strArray){
            strText += "  " + str;
        }

        tv_msg.setText(strText);

        LayoutInflater inflater = LayoutInflater.from(this);
        inflater.inflate(R.layout.my_textview,ll_layout);

        registerForContextMenu(tv_msg);//注册上下文菜单

    }

    //创建选项菜单（重写onCreateOptionsMenu方法）
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        menu.add(1,1,0,"new");
        menu.add(1,2,0,"open");
        menu.add(1,3,0,"insert");
        menu.add(1,4,0,"delete");

        return true;
    }

    //选项菜单项选中事件
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        return super.onOptionsItemSelected(item);
    }


    //创建上下文菜单（重写onCreateContextMenu方法）
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        MenuInflater inflater = new MenuInflater(this);
        inflater.inflate(R.menu.my_menus,menu);//嵌入菜单资源

//        super.onCreateContextMenu(menu, v, menuInfo);
    }

    //上下文菜单项选中事件
    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {

        if(item.getItemId() == R.id.menu_add){
            Toast.makeText(this, "add", Toast.LENGTH_SHORT).show();
        }
        if(item.getItemId() == R.id.menu_delete){
            Toast.makeText(this, "delete", Toast.LENGTH_SHORT).show();
        }
        if(item.getItemId() == R.id.menu_update){
            Toast.makeText(this, "update", Toast.LENGTH_SHORT).show();
        }
        if(item.getItemId() == R.id.menu_query){
            Toast.makeText(this, "query", Toast.LENGTH_SHORT).show();
        }

        return super.onContextItemSelected(item);
    }
}
