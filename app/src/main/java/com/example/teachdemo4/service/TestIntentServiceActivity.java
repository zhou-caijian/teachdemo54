package com.example.teachdemo4.service;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.JobIntentService;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.teachdemo4.R;

public class TestIntentServiceActivity extends AppCompatActivity {

    TextView tv_msg;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_intent_service);

        tv_msg = findViewById(R.id.tv_msg);
    }

    public void startService(View view) {

        Log.v("msg","点击了按钮");
        Intent intent = new Intent(TestIntentServiceActivity.this,MyIntentService.class);
        intent.putExtra("str_name","传递字符串");

        startService(intent);
    }

    public void startJobService(View view) {

        Intent intent = new Intent();
        intent.putExtra("str_name","传递字符串2");

        JobIntentService.enqueueWork(this,MyJobIntentService.class,0x110,intent);

    }

    ResultReceiver resultReceiver = new ResultReceiver(new Handler()){
        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            super.onReceiveResult(resultCode, resultData);

            if(resultCode == 0x112){
                Toast.makeText(TestIntentServiceActivity.this, resultData.getString("str_key"), Toast.LENGTH_SHORT).show();
                tv_msg.setText(resultData.getString("str_key"));
            }
        }
    };

    public void resultReceiver(View view) {

        Intent intent = new Intent();
        intent.putExtra("str_name","传递字符串3");
        intent.putExtra("receiver_key",resultReceiver);

        JobIntentService.enqueueWork(this,MyJobIntentService.class,0x111,intent);
        startActivity(new Intent(this,Main2Activity.class));

    }
}
