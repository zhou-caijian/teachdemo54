package com.example.teachdemo4.service;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.JobIntentService;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Filter;

import com.example.teachdemo4.R;

public class TestBroadcastActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_broadcast);
    }

    BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.v("广播接收",intent.getStringExtra("str_key"));
        }
    };

    @Override
    protected void onResume() {
        super.onResume();

        IntentFilter filter = new IntentFilter(MyJobIntentService.ACTION);
        registerReceiver(broadcastReceiver,filter);
    }

    @Override
    protected void onPause() {
        super.onPause();

        unregisterReceiver(broadcastReceiver);
    }



    public void go(View view) {
        Intent intent = new Intent();
        intent.putExtra("str_name","传递字符串7");

        JobIntentService.enqueueWork(this,MyJobIntentService.class,0x113,intent);

        startActivity(new Intent(this,TestBroadcast2Activity.class));
    }
}
