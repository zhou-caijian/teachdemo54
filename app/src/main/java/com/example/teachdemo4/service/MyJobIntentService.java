package com.example.teachdemo4.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.JobIntentService;
import androidx.core.app.NotificationCompat;

import com.example.teachdemo4.R;

public class MyJobIntentService extends JobIntentService {
    public static final String ACTION = "com.example.teachdemo4.xxx";
    @Override
    protected void onHandleWork(@NonNull Intent intent) {//当service启动时，执行此方法


//        if(intent!=null){
//            Log.v("msg",intent.getStringExtra("str_name"));
//        }

        /*
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        */
        /*
        if(intent!=null){

            Log.v("msg",intent.getStringExtra("str_name"));

            ResultReceiver resultReceiver = (ResultReceiver) intent.getExtras().get("receiver_key");

            Bundle bundle = new Bundle();
            bundle.putString("str_key","str_value");
            resultReceiver.send(0x112,bundle);
        }*/
        /*
        if(intent!=null){

            Log.v("msg",intent.getStringExtra("str_name"));

            Intent sendIntent = new Intent(ACTION);

            sendIntent.putExtra("str_key","str_value");

            sendBroadcast(sendIntent);

        }*/
        Log.v("test","2222");
        if(Build.VERSION.SDK_INT >= 26){
            String channelId = "channel1";
            String name = "0x110";

            NotificationChannel channel = new NotificationChannel(channelId,name,NotificationManager.IMPORTANCE_LOW);
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this,channelId);
            Notification notification = builder.setContentTitle("通知消息").setContentText("这是通知内容").setSmallIcon(R.mipmap.ic_launcher).build();

            NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            manager.createNotificationChannel(channel);
            manager.notify(0x110,notification);
        }else {

            NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
            Notification notification = builder
                    .setContentTitle("通知消息").
                    setContentText("这是通知内容")
                    .setSmallIcon(R.mipmap.ic_launcher)

                    .build();

            NotificationManager manager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

            manager.notify(0x110,notification);
        }

        //扩展：1、自定义通知样式--.setContent();2、点击通知可以可打开新的页面--setContentIntent()；3、在通知里面展示文件下载（进度、暂停、继续、取消），下载完成之后自动打开或者手动打开


    }
}
