package com.example.teachdemo4.service;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;

public class MyIntentService extends IntentService {
    /**
     * @param name
     * @deprecated
     */
    public MyIntentService(String name) {
        super(name);
    }

    public MyIntentService() {
        super("myservice");
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {//当service启动的时候会自动执行此方法，intent可以获取参数

        Log.v("service:","执行了onHandleIntent");

        if(intent != null){
            Toast.makeText(this, intent.getStringExtra("str_name"), Toast.LENGTH_SHORT).show();
        }

    }
}
