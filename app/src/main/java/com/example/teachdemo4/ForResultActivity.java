package com.example.teachdemo4;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class ForResultActivity extends AppCompatActivity {

    Button btn_get;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_for_result);
        btn_get = findViewById(R.id.btn_get);

        btn_get.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ForResultActivity.this,ResultActivity.class);

                String strValue = "Android";
                int intValue = 1010;

                Bundle bundle = new Bundle();
                bundle.putInt("int_key",intValue);
                bundle.putString("str_key",strValue);

                intent.putExtras(bundle);

                startActivityForResult(intent,0x010);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == 0x010 && resultCode == 0x011){
            if(data != null){
                Log.v("msg",data.getStringExtra("re_name"));
            }
        }
    }
}
