package com.example.teachdemo4;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.SeekBar;
import android.widget.TextView;

public class SeekBarActivity extends AppCompatActivity {

    SeekBar seekBar;
    TextView tvMsg;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seek_bar);

        seekBar = findViewById(R.id.seekBar);



        tvMsg = findViewById( R.id.tvMsg);


        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int value, boolean b) {

                tvMsg.setText("value:"+value);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                Log.v("start:","onStart");
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                Log.v("stop:","onStop");
            }
        });
    }
}
