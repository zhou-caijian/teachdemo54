package com.example.teachdemo4;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.teachdemo4.model.Student;
import com.example.teachdemo4.model.User;

import java.util.ArrayList;
import java.util.List;

public class Test2Activty extends AppCompatActivity {

    TextView tv_msg;
    Button btn_back;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test2);

        tv_msg = findViewById(R.id.tv_msg);
        btn_back = findViewById(R.id.btn_back);

        Intent intent = getIntent();

        Bundle bundle = intent.getExtras();

        tv_msg.setText(bundle.getString("str_key","哈哈，你拿不到我") + "   " + bundle.getInt("int_key",0));

        String[] strArray = bundle.getStringArray("array_key");


//        for(int i = 0;i< strArray.length;i++){
//            Log.v("data " + i," " + strArray[i]);
//        }
        int k = 0;
        for (String str:strArray) {

            Log.v("data " + k++," " + str);
        }

        ArrayList<String> list = bundle.getStringArrayList("list_key");
        for(String str:list){
            Log.v("data "," " + str);
        }


        User user = (User) bundle.getSerializable("user_key");

        Log.v("user ",user.toString());

        ArrayList<Student> studentArrayList = bundle.getParcelableArrayList("par_key");

        for(Student stu:studentArrayList){
            Log.v("data ",stu.toString());
        }

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });



    }
}
