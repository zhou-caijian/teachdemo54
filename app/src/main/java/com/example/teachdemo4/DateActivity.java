package com.example.teachdemo4;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.Toast;

import java.util.Calendar;

public class DateActivity extends AppCompatActivity {

    Button btnGetDate;
    CalendarView calDate;
    int year,month,day;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_date);

        btnGetDate = findViewById(R.id.btnGetDate);
        calDate = findViewById(R.id.calDate);
        long longDate = calDate.getDate();
        //将长整型时间转换成固定格式的时间

        Calendar calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH) + 1;
        day = calendar.get(Calendar.DAY_OF_MONTH);
        calDate.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView calendarView, int year, int month, int day) {
                DateActivity.this.year = year;
                DateActivity.this.month = month + 1;
                DateActivity.this.day = day;
                //Toast.makeText(DateActivity.this, ""+ year + "-" + (month+1) + "-" + day, Toast.LENGTH_SHORT).show();
            }
        });
        btnGetDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(DateActivity.this, "" + year + "年" + month + "月" + day + "日", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
